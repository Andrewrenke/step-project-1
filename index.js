const images = {
    graphicDesign: [
        img1 = {
            url: "images/graphic%20design/1.jpg",
            type: "Graphic Design"
        },
        img2 = {
            url: "images/graphic%20design/2.jpg",
            type: "Graphic Design"
        },
        img3 = {
            url: "images/graphic%20design/3.jpg",
            type: "Graphic Design"
        },
        img4 = {
            url: "images/graphic%20design/4.jpg",
            type: "Graphic Design"
        },
        img5 = {
            url: "images/graphic%20design/5.jpg",
            type: "Graphic Design"
        },
        img6 = {
            url: "images/graphic%20design/6.jpg",
            type: "Graphic Design"
        },
        // img7 = {
        //     url: "images/graphic%20design/7.jpg",
        //     type: "Graphic Design"
        // },
        // img8 = {
        //     url: "images/graphic%20design/8.jpg",
        //     type: "Graphic Design"
        // },
        // img9 = {
        //     url: "images/graphic%20design/9.jpg",
        //     type: "Graphic Design"
        // },
        // img10 = {
        //     url: "images/graphic%20design/10.jpg",
        //     type: "Graphic Design"
        // },
        // img11 = {
        //     url: "images/graphic%20design/11.jpg",
        //     type: "Graphic Design"
        // },
        // img12 = {
        //     url: "images/graphic%20design/12.jpg",
        //     type: "Graphic Design"
        // },
    ],
    webDesign: [
        img1 = {
            url: "images/web%20design/1.jpg",
            type: "Web Design"
        },
        img2 = {
            url: "images/web%20design/2.jpg",
            type: "Web Design"
        },
        img3 = {
            url: "images/web%20design/3.jpg",
            type: "Web Design"
        },
        img4 = {
            url: "images/web%20design/4.jpg",
            type: "Web Design"
        },
        img5 = {
            url: "images/web%20design/5.jpg",
            type: "Web Design"
        },
        img6 = {
            url: "images/web%20design/6.jpg",
            type: "Web Design"
        },
        // img7 = {
        //     url: "images/web%20design/7.jpg",
        //     type: "Web Design"
        // }
    ],
    landingPages: [
        img1 = {
            url: "images/landing%20page/1.jpg",
            type: "Landing Pages"
        },
        img2 = {
            url: "images/landing%20page/2.jpg",
            type: "Landing Pages"
        },
        img3 = {
            url: "images/landing%20page/3.jpg",
            type: "Landing Pages"
        },
        img4 = {
            url: "images/landing%20page/4.jpg",
            type: "Landing Pages"
        },
        img5 = {
            url: "images/landing%20page/5.jpg",
            type: "Landing Pages"
        },
        img6 = {
            url: "images/landing%20page/6.jpg",
            type: "Landing Pages"
        },
        // img7 = {
        //     url: "images/landing%20page/7.jpg",
        //     type: "Landing Pages"
        // }
    ],
    wordpress: [
        img1 = {
            url: "images/wordpress/1.jpg",
            type: "Wordpress"
        },
        img2 = {
            url: "images/wordpress/2.jpg",
            type: "Wordpress"
        },
        img3 = {
            url: "images/wordpress/3.jpg",
            type: "Wordpress"
        },
        img4 = {
            url: "images/wordpress/4.jpg",
            type: "Wordpress"
        },
        img5 = {
            url: "images/wordpress/5.jpg",
            type: "Wordpress"
        },
        img6 = {
            url: "images/wordpress/6.jpg",
            type: "Wordpress"
        },
        // img7 = {
        //     url: "images/wordpress/7.jpg",
        //     type: "Wordpress"
        // },
        // img8 = {
        //     url: "images/wordpress/8.jpg",
        //     type: "Wordpress"
        // },
        // img9 = {
        //     url: "images/wordpress/9.jpg",
        //     type: "Wordpress"
        // },
        // img10 = {
        //     url: "images/wordpress/10.jpg",
        //     type: "Wordpress"
        // }
    ]
}
const filterButtons = document.querySelectorAll(".work-section-nav-button");

const switchButtons = () => {
    const tabs = document.querySelectorAll('.section-nav-button');
    const tabImages = document.querySelectorAll('.tab-img');
    const tabContents = document.querySelectorAll('.tab-text');

    tabs.forEach(tab => {
        tab.addEventListener('click', (el) => {
            el.preventDefault();
            const tabId = tab.getAttribute('data-target');

            tabs.forEach(tab => {
                tab.classList.remove('active');
            });

            tabContents.forEach(tabContent => {
                tabContent.style.display = "none";
            });

            tabImages.forEach((img) => {
                img.style.display = "none";
            });

            tab.classList.add('active');
            const activeTab = document.querySelector(`.tab-text[data-target="${tabId}"]`);
            const activeImg = document.querySelector(`.tab-img[data-target="${tabId}"]`);
            activeTab.style.display = "block";
            activeImg.style.display = "block";
        });
    });
}

switchButtons();

const switchImages = () => {
    filterButtons.forEach((tab) => {
        const activeTab = tab.getAttribute("data-type");
        tab.addEventListener("click", () => {
            filterButtons.forEach(btn => {
                btn.classList.remove('active-filter');
            });
            tab.classList.add('active-filter');
            printCard(activeTab);
        });
    });
}

switchImages();


function printCard(category) {
    const imgContainer = document.querySelector(".image-container");
    const loadMoreButton = document.createElement("button");
    let currentArray = [];
    imgContainer.innerHTML = "";
    currentArray = [...images["graphicDesign"], ...images["wordpress"]];

    if (category === "all") {
        loadMoreButton.style.display = "flex";
    } else {
        currentArray = images[category];
        loadMoreButton.style.display = "none";
    }

    currentArray.forEach((key) => {
        imgContainer.innerHTML += `
                <div class="image-wrapper">
                    <img src=${key.url} alt="">
                    <div class="gallery-hover-action">
                        <div class="gallery-button-wrapper">
                            <div class="copy-link" title="Copy Link">
                                <img src="images/copy-link-image.png" alt="">
                            </div>
                            <div class="download-image" title="Download Image">
                                <div></div>
                            </div>
                        </div>
                        <h3>creative design</h3>
                        <p>${key.type}</p>
                    </div>
            </div>`
    });

    loadMoreButton.addEventListener("click", () => {
        loadMoreButton.style.display = "none";
        currentArray = [...images["landingPages"], ...images["webDesign"]];
        currentArray.forEach((key) => {
            imgContainer.innerHTML += `
                <div class="image-wrapper">
                    <img src=${key.url} alt="">
                    <div class="gallery-hover-action">
                        <div class="gallery-button-wrapper">
                            <div class="copy-link" title="Copy Link">
                                <img src="images/copy-link-image.png" alt="">
                            </div>
                            <div class="download-image" title="Download Image">
                                <div></div>
                            </div>
                        </div>
                        <h3>creative design</h3>
                        <p>${key.type}</p>
                    </div>
            </div>`
        });
    });
        loadMoreButton.classList.add("load-more-button");
        loadMoreButton.textContent = "Load More";
        imgContainer.appendChild(loadMoreButton);
}

printCard("all");
